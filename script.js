/*  Метод forEach перебирает элементы массива, как это можно было бы организовать  при помощи цикла 'for'.  Этот метод не возвращает значения. Перебирая значения массива в порядке возрастания индекса он (метод) позволяет выполнить переданную функцию 'callback' один раз для каждого элемента. Особенностью и одним из отличиев от цикла 'for' является невозможность использования оператора 'break' для досрочного выхода.
 */

const arr = [
  "hello",
  "world",
  23,
  0,
  48,
  "23",
  true,
  false,
  undefined,
  123n,
  null,
  {},
];

const typeData = "string";
const newArr = filterBy(arr, typeData); // creating a typed type using the 'filter' method
const newArrSecondary = filterByAlternative(arr, typeData); // creating a typed type using the 'forEach' method

const allTypes = [
  "string",
  "bigint",
  "boolean",
  "undefined",
  "object",
  "number",
];
// testing a function with a 'filter' method with different data types
allTypes.forEach((type) => console.log(filterBy(arr, type)));
// testing a function with a 'forEach' method with different data types
allTypes.forEach((type) => console.log(filterByAlternative(arr, type)));

function filterBy(a, b) {
  let typedArr = a.filter(function (a) {
    return typeof a === b && a !== null; // excluding 'null' from data type 'object'
  });
  return typedArr;
}

function filterByAlternative(a, b) {
  let currentArr = [];
  a.forEach(function (a) {
    if (typeof a === b && a !== null) {
      // excluding 'null' from data type 'object'
      currentArr.push(a);
    }
  });
  return currentArr;
}
